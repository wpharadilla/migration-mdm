import logging
import os
import urllib.parse
from utils.common import *
from sqlalchemy import create_engine
from .conn import mdm_dev_dest_engine


LOG_FORMAT = '[%(asctime)s] %(levelname)s %(filename)s line:%(lineno)d\t- %(message)s'
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)

def main():
    basedir = os.getcwd()
    abs_path = os.path.abspath(__file__)
    module = abs_path.replace(basedir + '/', '')
    search_path = os.path.join(os.getcwd(), 'CV_DAYA_ABADI_MAS-1573/sql')
    sales_office_code = "D326"

    # Get from raw data
    PATH = "CV_DAYA_ABADI_MAS-1573/data"
    recipes = [
        {
            "file_name": "D326 ZDRSALES.xlsx",
            "converter": {'DO Number':str}
        },
        {
            "file_name": "D326 AR SA open.xlsx",
            "converter": {}
        },
    ]

    # Load raw data to SQL
    for r in recipes:
        file_name = r.get("file_name")
        logging.info('Processing file: %s' % file_name)
        file_path = os.path.join(PATH, file_name)
        excel_to_sql(path=file_path, engine=mdm_dev_dest_engine)

    # Indexing
    rs = execute_sql(searchpath=search_path,
                     template_name='raw_data_indexing.sql',
                     engine=mdm_dev_dest_engine,
                     dict_vars={"sales_office_code": sales_office_code})

if __name__ == "__main__":
    main()
