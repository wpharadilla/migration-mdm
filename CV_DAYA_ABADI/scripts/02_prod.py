import logging
import os
import pathlib
from . import load
from .conn import src_engine, fct_dest_engine
from utils.common import execute_sql, transfer_rdbms_to_rdbms, IncrementSeq, transfer_rdbms_to_rdbms_std
# from . import somp

LOG_FORMAT = '[%(asctime)s] %(levelname)s %(filename)s line:%(lineno)d\t- %(message)s'
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)

def main():
    basedir = os.getcwd()
    abs_path = os.path.abspath(__file__)
    module = abs_path.replace(basedir + '/', '')
    search_path = os.path.join(os.getcwd(), '01_D326/order_to_cash/sql')
    sales_office_code = "D326"

    seq = IncrementSeq()
    dry_run = True  # TODO


    # TODO
    logging.info(
        f"# Sequence 1.{seq.add()}: Clean up on target tables based on list of billing as in SAP report to")
    rs = execute_sql(searchpath=search_path,
                     template_name='clean_up_related_tables_fct_prod.sql',
                     engine=fct_dest_engine,
                     dict_vars={"sales_office_code": "D326"})

    logging.info(
        f"# Sequence 1.{seq.add()}: Transform to transform_fctdb_prod_bdp_fct_billing")
    rs = execute_sql(searchpath=search_path,
                     template_name='transform_fct_prod__bdp_fct_billing.sql',
                     engine=src_engine,
                     dict_vars={"sales_office_code": sales_office_code})

    #logging.info(
    #    f"# Sequence 1.{seq.add()}: Load to fctdb_prod_bdp_fct_billing")
    #transfer_rdbms_to_rdbms_std(
    #    searchpath=search_path, template_name='data_transfer.sql',
    #    src_engine=src_engine, dest_engine=fct_dest_engine,
    #    if_exists='append',
    #    chunksize=100,
    #    # use_pandas=True,
    #    dict_vars={
    #        "source_table": f"transform_{sales_office_code }_fct_prod__bdp_fct_billing",
    #        "dest_table": "bdp_fct_billing",
    #        "field_list": ['sales_office_code','sales_office_name','sold_to_party_id','sold_to_party_code','sold_to_party_name','sold_to_party_address','ktp_no','npwp_no','po_no','po_date','so_no','so_id','so_amount','do_no','pod_no','billing_no','billing_date','doc_status','top_code_sys','due_date','incoterms','tax_status','salesman_code','salesman_name','gross_total','discount_total','subtotal','down_payment','dpp_amount','tax_amount','total','created_at','created_by','updated_at','updated_by','do_date','so_date','company_id','sales_office_id','sales_org_id','billing_type','deletion_flag','cancel_indicator','salesorg_code','payment_status','is_tax_invoice_created','ship_to_party_id','ship_to_party_code','ship_to_party_name','sales_office_address','tokosmart_points_used','company_name','salesorg_name','currency_code','pod_date','dist_channel_code','cancel_reason','posting_date','posting_period','doc_ref_reverse','cogs_total_amt', ]
    #    }
    #)

    logging.info(f"# Sequence 1.{seq.add()}: Load to fctdb_prod_bdp_fct_billing")
    insert_stmt = """INSERT INTO bdp_fct_billing ( sales_office_code ,sales_office_name ,sold_to_party_id ,sold_to_party_code ,sold_to_party_name ,sold_to_party_address ,ktp_no ,npwp_no ,po_no ,po_date ,so_no ,so_id ,so_amount ,do_no ,pod_no ,billing_no ,billing_date ,doc_status ,top_code_sys ,due_date ,incoterms ,tax_status ,salesman_code ,salesman_name ,gross_total ,discount_total ,subtotal ,down_payment ,dpp_amount ,tax_amount ,total ,created_at ,created_by ,updated_at ,updated_by ,do_date ,so_date ,company_id ,sales_office_id ,sales_org_id ,billing_type ,deletion_flag ,cancel_indicator ,salesorg_code ,payment_status ,is_tax_invoice_created ,ship_to_party_id ,ship_to_party_code ,ship_to_party_name ,sales_office_address ,tokosmart_points_used ,company_name ,salesorg_name ,currency_code ,pod_date ,dist_channel_code ,cancel_reason ,posting_date ,posting_period ,doc_ref_reverse ,cogs_total_amt ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
    transfer_rdbms_to_rdbms_std(
        searchpath=search_path, template_name='data_transfer.sql',
        src_engine=src_engine, dest_engine=fct_dest_engine,
        insert_stmt=insert_stmt,
        dict_vars={
            "source_table": f"transform_{sales_office_code }_fct_prod__bdp_fct_billing",
            "dest_table": "bdp_fct_billing",
            "field_list": ['sales_office_code','sales_office_name','sold_to_party_id','sold_to_party_code','sold_to_party_name','sold_to_party_address','ktp_no','npwp_no','po_no','po_date','so_no','so_id','so_amount','do_no','pod_no','billing_no','billing_date','doc_status','top_code_sys','due_date','incoterms','tax_status','salesman_code','salesman_name','gross_total','discount_total','subtotal','down_payment','dpp_amount','tax_amount','total','created_at','created_by','updated_at','updated_by','do_date','so_date','company_id','sales_office_id','sales_org_id','billing_type','deletion_flag','cancel_indicator','salesorg_code','payment_status','is_tax_invoice_created','ship_to_party_id','ship_to_party_code','ship_to_party_name','sales_office_address','tokosmart_points_used','company_name','salesorg_name','currency_code','pod_date','dist_channel_code','cancel_reason','posting_date','posting_period','doc_ref_reverse','cogs_total_amt', ]
        }
    )

    logging.info(
        f"# Sequence 1.{seq.add()}: Get PK and dependencies fields from fctdb_prod_bdp_fct_billing")
    transfer_rdbms_to_rdbms(
        searchpath=search_path, template_name='data_transfer.sql',
        src_engine=fct_dest_engine, dest_engine=src_engine,
        if_exists='replace', use_pandas=True,
        dict_vars={
            "source_table": "bdp_fct_billing",
            "dest_table": f"{sales_office_code}_fct_prod__bdp_fct_billing",
            "field_list": ['id', 'billing_no', ]
            # "field_list": ['*', ]
        }
    )

    logging.info(f"# Sequence 1.{seq.add()}: Transform to transform_fctdb_prod_bdp_fct_billing_detail1")
    rs = execute_sql(searchpath=search_path,
                     template_name='transform_fct_prod__bdp_fct_billing_detail1.sql',
                     engine=src_engine,
                     dict_vars={"sales_office_code": sales_office_code})

    logging.info(f"# Sequence 1.{seq.add()}: Load to so_mgt_portal_prod_bdp_so_custorder_detail1")
    transfer_rdbms_to_rdbms(
        searchpath=search_path, template_name='data_transfer.sql',
        src_engine=src_engine, dest_engine=fct_dest_engine,
        if_exists='append', use_pandas=True,
        dict_vars={
            "source_table": f"transform_{sales_office_code}_fct_prod__bdp_fct_billing_detail1",
            "dest_table": "bdp_fct_billing_detail1",
            "field_list": ['bill_id','billing_no','item_no','product_id','product_code','product_name','product_category','quantity','product_uom','uom_composite','base_price','price_value','disc_percent','disc_amt','down_payment','dpp_amount','tax_amount','subtotal_amt','cogs','parent_combo_code','remain_cap','reason_id','rejection_code','deletion_flag','created_at','created_by','updated_at','updated_by','gross_value','principal_id','principal_code','principal_name', ]
        }
    )

    logging.info(f"# Sequence 1.{seq.add()}: Get PK and dependencies fields from fctdb_prod_bdp_fct_billing_detail1")
    # transfer_rdbms_to_rdbms(
    #     searchpath=search_path, template_name='data_transfer.sql',
    #     src_engine=somp_dest_engine, dest_engine=src_engine,
    #     if_exists='replace', use_pandas=True,
    #     dict_vars={
    #         "source_table": "bdp_so_custorder_detail1",
    #         "dest_table": f"{sales_office_code}_so_mgt_portal_prod__bdp_so_custorder_detail1",
    #         "field_list": ['id', 'order_no', ]
    #         # "field_list": ['*', ]
    #     }
    # )

    logging.info(f"# Sequence 1.{seq.add()}: Transform to transform_fctdb_prod_bdp_fct_billing_detail3")
    rs = execute_sql(searchpath=search_path,
                     template_name='transform_fct_prod__bdp_fct_billing_detail3.sql',
                     engine=src_engine,
                     dict_vars={"sales_office_code": sales_office_code})

    logging.info(f"# Sequence 1.{seq.add()}: Load to fctdb_prod_bdp_fct_billing_detail3")
    transfer_rdbms_to_rdbms(
        searchpath=search_path, template_name='data_transfer.sql',
        src_engine=src_engine, dest_engine=fct_dest_engine,
        if_exists='append', use_pandas=True,
        dict_vars={
            "source_table": f"transform_{sales_office_code}_fct_prod__bdp_fct_billing_detail3",
            "dest_table": "bdp_fct_billing_detail3",
            "field_list": ['item_no','bill_id','product_code','quantity','product_uom','batch_no','expiry_date','storace_location_id','created_at','created_by','updated_at','updated_by','gross_value','disc_amt','storage_location_id','item_no_sys', ]
        }
    )

    logging.info(f"# Sequence 1.{seq.add()}: Get PK and dependencies fields from fctdb_prod_bdp_fct_billing_detail3")
    # transfer_rdbms_to_rdbms(
    #     searchpath=search_path, template_name='data_transfer.sql',
    #     src_engine=somp_dest_engine, dest_engine=src_engine,
    #     if_exists='replace', use_pandas=True,
    #     dict_vars={
    #         "source_table": "bdp_so_custorder_detail1",
    #         "dest_table": f"{sales_office_code}_so_mgt_portal_prod__bdp_so_custorder_detail1",
    #         "field_list": ['id', 'order_no', ]
    #         # "field_list": ['*', ]
    #     }
    # )

if __name__ == "__main__":
    main()
