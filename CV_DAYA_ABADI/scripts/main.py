import logging
import os
import pathlib
from utils.common import execute_sql, transfer_rdbms_to_rdbms
from . import load
from . import listing
from . import dev_somp, dev_tms, dev_idsp, dev_fct, dev_upd, cleanup


LOG_FORMAT = '[%(asctime)s] %(levelname)s %(filename)s line:%(lineno)d\t- %(message)s'
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)

if __name__ == "__main__":
    basedir = os.getcwd()
    abs_path = os.path.abspath(__file__)
    module = abs_path.replace(basedir + '/', '')
    search_path = os.path.join(os.getcwd(), '01_D326/order_to_cash/sql')
    sales_office_code = "D326"

    # Sequence 1: Load raw data to SQL Server
    #load.main()

    # Sequence 1: Load raw data to SQL Server
    cleanup.main()
    
    # # Sequence 1: Prepare migration list
    listing.main()

    # # Sequence 1: SOMP
    dev_somp.main()

    # # Sequence 2: TMS
    dev_tms.main()

    # # Sequence 3: IDSP
    dev_idsp.main()

    # # Sequence 4: FCT
    dev_fct.main()

    # # # Sequence 5: update
    dev_upd.main()

'''
clear && time python -m 01_D326.order_to_cash.scripts.main

'''
