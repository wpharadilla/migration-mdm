import jinja2
import logging
import os
import pandas as pd
import re
import urllib.parse
from sqlalchemy import create_engine, event
from sqlalchemy.sql import text


LOG_FORMAT = '[%(asctime)s] %(levelname)s %(filename)s line:%(lineno)d\t- %(message)s'
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)


def clean_file_name(file_name, replacement='_', lowercase=True):
    return re.sub("[^0-9a-zA-Z]+", replacement, file_name).lower()


def excel_to_sql(path, engine, converter=None, limit=None):

    @event.listens_for(engine, "before_cursor_execute")
    def receive_before_cursor_execute(conn, cursor, statement, params, context, executemany):
        if executemany:
            cursor.fast_executemany = True

    if converter is not None:
        df = pd.read_excel(path, convert_float=True, converters=converter)
    else:
        df = pd.read_excel(path, convert_float=True)

    # load dataframe to SQL
    table_name = clean_file_name(os.path.split(path)[-1].split('.')[0])
    logging.info('Loading from file %s to table %s' % (path, table_name))

    if limit is not None:
        df.head(limit).to_sql(table_name, engine, if_exists='replace')
    else:
        df.to_sql(table_name, engine, if_exists='replace')


def render_sql_template(searchpath, template_name, dict_vars={}):
    if template_name.endswith(".sql"):
        templateLoader = jinja2.FileSystemLoader(searchpath=searchpath)
        templateEnv = jinja2.Environment(loader=templateLoader)
        template = templateEnv.get_template(template_name)
        rendered = template.render(**dict_vars)
    else:
        template = jinja2.Environment(
            loader=jinja2.BaseLoader()).from_string(template_name)
        rendered = template.render(**dict_vars)

    return rendered


def execute_sql(searchpath, template_name, engine, dict_vars={}):
    sql = render_sql_template(searchpath, template_name, dict_vars)
    logging.info("Executing SQL:\n" + sql)
    r = engine.execute(text(sql).execution_options(autocommit=True))

    try:
        result = r.fetchall()
    except Exception as e:
        result = list()

    return result


def pandas_read_sql(sql, engine):
    df = pd.read_sql(sql, engine)
    return df


def transfer_rdbms_to_rdbms(searchpath, template_name, src_engine, dest_engine, if_exists='append', use_pandas=True, dict_vars={}):

    @event.listens_for(dest_engine, "before_cursor_execute")
    def receive_before_cursor_execute(conn, cursor, statement, params, context, executemany):
        if executemany:
            cursor.fast_executemany = True

    dest_table = dict_vars['dest_table']

    if use_pandas:
        # Render SQL
        sql = render_sql_template(searchpath, template_name, dict_vars)
        logging.info("Running SQL")
        print(sql)
        print()

        # Get data from SQL to Pandas dataframe
        df = pandas_read_sql(sql, src_engine)

        # Load data using Pandas transform_sql
        logging.info("Using connection: %s" % dest_engine)
        logging.info(f"Transferring into table {dest_table}")
        df.to_sql(dest_table, dest_engine, if_exists=if_exists, index=False)

        logging.info(f"Done transfer into table {dest_table}")


def transfer_rdbms_to_rdbms_std(searchpath, template_name, src_engine, dest_engine, insert_stmt=None, dict_vars={}):
    dest_table = dict_vars['dest_table']
    # Render SQL
    sql = render_sql_template(searchpath, template_name, dict_vars)
    logging.info("Running SQL")
    print(sql)
    print()
    # Get data from SQL to Pandas dataframe
    df = pandas_read_sql(sql, src_engine)
    # Load data using Pandas transform_sql
    logging.info("Using connection: %s" % dest_engine)
    logging.info(f"Transferring into table {dest_table}")
    connection = dest_engine.raw_connection()
    cursor = connection.cursor()
    records_array = df.to_records(index=False)
    records = [tuple(x) for x in records_array]
    for r in records:
        cursor.execute(insert_stmt, r)
        connection.commit()
    cursor.close()
    connection.close()
    logging.info(f"Done transfer into table {dest_table}")


def transfer_rdbms_to_rdbms_return_pk(searchpath, template_name, src_engine, dest_engine, insert_stmt=None, dict_vars={}):
    source_table = dict_vars['source_table']
    dest_table = dict_vars['dest_table']
    return_pk_table = dict_vars['return_pk_table']
    bussiness_keys = dict_vars['bussiness_keys']
    field_list = dict_vars['field_list']
    returning_data = list()
    # Render SQL
    sql = render_sql_template(searchpath, template_name, dict_vars)
    logging.info("Running SQL")
    print(sql)
    print()

    with src_engine.connect() as src_conn, dest_engine.connect() as dest_conn:
        records = src_conn.execute(sql)
        cnt = 0
        for r in records:
            # for each row from SQL Server, we insert it into MySQL and get the PK
            tup = list()

            # Before insert to MySQL, we need to remove the bussiness keys
            row_to_insert = [r[k]
                             for k in field_list if k not in bussiness_keys]

            # insert_data to mysql
            res = dest_conn.execute(insert_stmt, row_to_insert)

            # get last row id inserted
            tup.append(res.lastrowid)

            # Get the list of bussiness key's value
            bussiness_key_values = [r[k] for k in bussiness_keys]
            tup.extend(bussiness_key_values)
            returning_data.append(tup)

        # Now we already have the ID from MySQL + bussiness keys
        # Let's insert it back to SQL Server
        df = pd.DataFrame(returning_data, columns=[
                          'id'].extend(bussiness_keys))
        df.to_sql(return_pk_table, src_conn, if_exists='replace')

        src_conn.close()
        dest_conn.close()

    logging.info(f"Done transfer into table {dest_table}")
    

def import_data(searchpath, raw_data, table_name, engine, separator,):
    # Replace Excel_file_name with your excel sheet name
    df = pd.read_csv(searchpath + raw_data, sep=separator,
                        quotechar='\'', encoding='utf8')
    # Replace Table_name with your sql table name
    df.to_sql(table_name, engine, index=False, if_exists='replace',)
    logging.info("Import data:\n" + raw_data + " to " + table_name)


class IncrementSeq(object):
    def __init__(self):
        super().__init__()
        self.incr = 0

    def add(self):
        self.incr = self.incr + 1
        return self.incr
