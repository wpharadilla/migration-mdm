import os
import urllib.parse
from utils.common import *
from sqlalchemy import create_engine

# DEV
mdm_dev_host = "dev.db.tokosmart.id"
mdm_dev_password = "2Wd@TaEn6078"
mdm_dev_port = "3306"
mdm_dev_username = "rw_dataengineer"
mdm_dev_database = "master_data_dev"
mdm_dev_dest_conn_string = f"mysql+mysqldb://{mdm_dev_username}:{urllib.parse.quote_plus(mdm_dev_password)}@{mdm_dev_host}:{mdm_dev_port}/{mdm_dev_database}"
mdm_dev_dest_engine = create_engine(mdm_dev_dest_conn_string, pool_recycle=3600, pool_pre_ping=True)

# UAT
mdm_uat_host = "dev.db.tokosmart.id"
mdm_uat_password = "2Wd@TaEn6078"
mdm_uat_port = "3306"
mdm_uat_username = "rw_dataengineer"
mdm_uat_database = "master_data_uat"
mdm_uat_dest_conn_string = f"mysql+mysqldb://{mdm_uat_username}:{urllib.parse.quote_plus(mdm_uat_password)}@{mdm_uat_host}:{mdm_uat_port}/{mdm_uat_database}"
mdm_uat_dest_engine = create_engine(mdm_uat_dest_conn_string, pool_recycle=3600, pool_pre_ping=True)

# PROD
mdm_prod_host = "db-mdm-prod.tokosmart.id"
mdm_prod_password = "2Wd@TaEn6078"
mdm_prod_port = "3306"
mdm_prod_username = "rw_dataengineer"
mdm_prod_database = "masterdatadb_prod"
mdm_prod_dest_conn_string = f"mysql+mysqldb://{mdm_prod_username}:{urllib.parse.quote_plus(mdm_prod_password)}@{mdm_prod_host}:{mdm_prod_port}/{mdm_prod_database}"
mdm_prod_dest_engine = create_engine(mdm_prod_dest_conn_string, pool_recycle=3600, pool_pre_ping=True)